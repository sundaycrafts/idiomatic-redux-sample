import registerServiceWorker from './registerServiceWorker';
import './reducers/counter'
import React from 'react'
import ReactDOM from 'react-dom'
import Counter from './components/Counter'
import configureStore from './configureStore'
import * as actions from './actions'

const store = configureStore()

const render = () => {
  ReactDOM.render(
    <Counter
      value={store.getState()}
      onIncrement={() => store.dispatch(actions.increment(1))}
      onDecrement={() => store.dispatch(actions.decrement(1))}
      onAsync={() => store.dispatch(actions.thunk(1))}
    />,
    document.getElementById('root')
  )
}

store.subscribe(render)
render()

registerServiceWorker();
