import counter from './counter'
import * as action from '../actions'

it('counter test', () => {
  expect(counter(0, action.increment(1))).toEqual(1)
  expect(counter(1, action.increment(1))).toEqual(2)
  expect(counter(2, action.decrement(1))).toEqual(1)
  expect(counter(1, action.decrement(1))).toEqual(0)
  expect(counter(0, { type: 'SOMETHING_ELSE' })).toEqual(0)
  expect(counter(undefined, {})).toEqual(0)
})
