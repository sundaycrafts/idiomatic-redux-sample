import { handleActions, combineActions } from 'redux-actions'
import * as actions from '../actions'

const counter = handleActions({
  [combineActions(actions.increment, actions.decrement)](state, { payload: { amount } }) {
    return state + amount
  }
}, 0)

export default counter
