import React from 'react'

const Counter = ({
  value,
  onIncrement,
  onDecrement,
  onAsync
}) => (
  <div>
    <h1>{value}</h1>
    <button onClick={onIncrement}>+</button>
    <button onClick={onDecrement}>-</button>

    <div>
      <button onClick={onAsync}>Asynchronous</button>
    </div>
  </div>
)

export default Counter
