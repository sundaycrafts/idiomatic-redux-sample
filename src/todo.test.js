import { toggleTodo } from './todo'
import { Map } from 'immutable'
import * as matchers from 'jest-immutable-matchers'

beforeEach(() => jest.addMatchers(matchers))

it('Todo test', () => {
  const todoBefore = Map({
    id: 0,
    text: 'Learn Redux',
    completed: false
  })

  const todoAfter = Map({
    id: 0,
    text: 'Learn Redux',
    completed: true
  })

  expect(toggleTodo(todoBefore)).toEqualImmutable(todoAfter)
})
