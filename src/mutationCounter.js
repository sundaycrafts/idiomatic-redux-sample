export const addCounter = list => list.set(-1, 0)
export const removeCounter = (list, index) => list.delete(index)
export const incrementCounter = (list, index) => list.update(index, value => ++value)
