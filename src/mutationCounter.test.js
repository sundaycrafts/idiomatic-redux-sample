import { addCounter, removeCounter, incrementCounter } from './mutationCounter'
import { List } from 'immutable'
import * as matchers from 'jest-immutable-matchers'

describe('Test suite', () => {
  beforeEach(() => jest.addMatchers(matchers))

  it('test addCounter', () => {
    const listBefore = List([])
    const listAfter = List([0])

    expect(
      addCounter(listBefore)
    ).toEqualImmutable(listAfter)
  })

  it('test removeCounter', () => {
    const listBefore = List([0, 10, 20])
    const listAfter = List([0, 20])

    expect(
      removeCounter(listBefore, 1)
    ).toEqualImmutable(listAfter)
  })

  it('test incrementCounter', () => {
    const listBefore = List([0, 10, 20])
    const listAfter = List([0, 11, 20])

    expect(
      incrementCounter(listBefore, 1)
    ).toEqualImmutable(listAfter)
  })
})
