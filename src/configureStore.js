import { createStore, applyMiddleware } from 'redux'
import counter from './reducers/counter'
import thunk from 'redux-thunk'

const configureStore = () => {
  const middlewares = [thunk]

  const store = createStore(
    counter,
    applyMiddleware(...middlewares)
  )
  return store
}

export default configureStore
