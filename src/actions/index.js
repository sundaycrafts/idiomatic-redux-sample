import { createActions } from 'redux-actions'

export const { increment, decrement } = createActions({
  INCREMENT: amount => ({ amount }),
  DECREMENT: amount => ({ amount: -amount })
})

export const thunk = amount => dispatch => {
  console.log('Now using redux-thunk with redux-actions🎉')

  setTimeout(() => {
    console.log('Dispatched action in Asynchronous.')
    dispatch(increment(100))
  }, 500)
}
