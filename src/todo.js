export const toggleTodo = todo => todo.update('completed', bool => !bool)
